---
title: Home
banner_image: "/uploads/2018/08/03/home-bg-40-short-1.jpg"
layout: landing-page
heading: Rethinking day-to-day job management
partners:
- "/uploads/2017/11/13/stem.png"
- "/uploads/2017/11/13/UPenn_logo.png"
- "/uploads/2017/11/13/nysed.png"
services:
- description: No more chasing staff, duplicating paperwork or waiting for engineers
    to come back to the office to pick up or drop off job sheets.
  heading: Saves you time
  icon: "/uploads/2018/08/03/icon-sandclock-sterling.png"
- description: No missed jobs, disputed invoices or missed invoices.  Raise an invoice
    as soon as your job is complete and get paid faster.
  heading: Reduce your costs
  icon: "/uploads/2018/08/03/icon-piggy-bank.png"
- description: Respond quickly and proactively to customer queries.  All your information
    is in one place and immediately available.
  icon: "/uploads/2018/08/03/icon-link-users.png"
  heading: Improve your service
sub_heading: Join 20,000 of the Best Companies on Okappy Start Saving Your Time and
  Money With Electronic Job Sheets.
textline: |
  Belkirk College of Engineering

  Ora et Labora
hero_button:
  text: Learn more
  href: "/about"
show_news: true
show_staff: false
menu:
  navigation:
    identifier: _index
    weight: 1

---
